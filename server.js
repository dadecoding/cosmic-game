var express = require('express'),
    hogan = require('hogan-express'),
    path = require('path'),
    fs = require('fs'),
    app = express();

var app = express();

app.set('view engine', 'html');
app.set('partials', { header: 'partials/header', footer: 'partials/footer' });
app.engine('html', require('hogan-express'));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res) {

  res.render('index');

});

var ships = new Object;

var io = require('socket.io').listen(app.listen(3001));

io.on('connection', function(socket) {
  console.log('Ship connected');

  socket.on('ship named', function(shipname) {
    console.log('Ship logged in as:', shipname);

    ships[shipname] = {};
    console.log(ships);

    io.sockets.emit('create ship', ships);
  });

  socket.on('ship move', function(shipdata) {
    socket.broadcast.emit('move ship', shipdata);
  });
});

var prev = [];
setInterval(function() { prev = []; }, 500);

var CosmicShooting = new Class({
  initialize: function() {
    this.config = CosmicConfig.config;
    this.preload();
  },
  preload: function() {
    var cfg = this.config;
    var bullets = cfg.shooting.bullets;

    bullets = Cosmic.game.add.group();
    bullets.createMultiple(20, 'bullet');
    cfg.shooting.bullets = bullets;

    cfg.shooting.explosion = Cosmic.game.add.sprite(0, 0, 'explosion');
    cfg.shooting.explosion.animations.add('explode', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0], 5, false);

  },
  fire: function() {
    var cfg = CosmicConfig.config;
    var bullets = cfg.shooting.bullets;
    var currentPlayer = cfg.players.list[cfg.players.current];

    if (Cosmic.game.time.now > cfg.shooting.bulletTime) {
      cfg.shooting.bulletTime = Cosmic.game.time.now + cfg.shooting.config.rate;
      var bullet = bullets.getFirstExists(false);
      if (bullet) {
        bullet.exists = true;
        bullet.name = 'bullet';
        bullet.lifespan = 2500;
        Cosmic.game.physics.p2.enable(bullet);
        //bullet.body.data.shapes[0].sensor = true;
        bullet.body.setCircle(6);
        //bullet.body.debug = true;

        bullet.reset(currentPlayer.x + 10, currentPlayer.y + 10);
        var bulletAngle = currentPlayer.body.angle;
        var bulletAngleRad = bulletAngle * Math.PI / 180;
        var bulletAngleRad2 = (bulletAngle - 90) * Math.PI / 180;

        var bulletProperAngle = (currentPlayer.body.angle - 90) * Math.PI / 180;

        bullet.body.x = currentPlayer.x + (Math.cos(bulletProperAngle) * 26);
        bullet.body.y = currentPlayer.y + (Math.sin(bulletProperAngle) * 26);

        bullet.body.velocity.x = Math.cos(bulletProperAngle) * 400;
        bullet.body.velocity.y = Math.sin(bulletProperAngle) * 400;

        bullet.body.onBeginContact.add(this.targetHit);
      }
    }
  },
  targetHit: function(body, shapea, shapeb) {

    var target;
    if (body) {
      target = body.sprite;
      if (target.name == 'asteroid') {
        if (prev[0] != shapea.id && prev[1] != shapeb.id) {
          prev = [];
          prev.push(shapea.id);
          prev.push(shapeb.id);

          if (target.hp == 1) {
            target.kill();
            if (target.level != 3) {
              Asteroids.devideAsteroid(target.level);
            }
          } else {
            target.frame++;
            target.hp--;
          }
        } else { console.log('double'); }
      }

    }
  }
});

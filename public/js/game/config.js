var CosmicConfig = new Singleton({
  initialize: function() {

  },
  createConfig: function() {
    this.config = {
      game: {
        map: null,
        layer: null,
        cursors: null,
        backroundColor: '#000', // Background of the canvas
        worldWidth: 800, // Width of the entire world (Not canvas)
        worldHeight: 600, // Height of the entire world (Not canvas)
        cursors: null
      },
      players: {
        current: null,
        list: []
      },
      ship: {
        facing: 'up',
        speed: {
          current: null,
          top: 100, // How fast ship flies
          turn: 150, // How fast ship turnes
          break: 10 // How fast ship stops
        }

      },
      shooting: {
        bullet: null,
        bullets: null,
        bulletTime: 0,
        config: {
          rate: 500, // How often ship shoots
          range: 4000, // How far bullet flies
          speed: 200 // How fast bullet flies
        },
        explosion: null
      },
      asteroids: {
        list: null,
        number: 3, // How many asteroids should be on the map
        topSpeed: 50 // How fast asteroids can move
      },
      collisionGroups: {
        playerCG: null,
        asteroidsCG: null,
        bulletsCG: null
      },
      powerups: {
        list: null,
      }
    }
  },
  get: function() {
    return this.config;
  }
});

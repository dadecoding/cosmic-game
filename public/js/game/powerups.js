var testtttt = 0;
var CosmicPowerups = new Class({
  initialize: function() {
    this.config = CosmicConfig.config;
    this.preload();
    this.shield;
  },
  preload: function() {
    this.generatePowerup();
  },
  generatePowerup: function() {
    var cfg = CosmicConfig.config;
    var powerups = cfg.powerups.list;
    powerups = Cosmic.game.add.group();

    for (var i=1; i<3; i++) {
      var p, x, y = 500;
      if (i == 1) {
        p = 'powerup-shield';
        x = 600;
      }
      else if (i == 2) {
        p = 'powerup-health';
        x = 500;
      }

      var powerup = powerups.create(x, y, p);
      powerup.name = p;
      Cosmic.game.physics.p2.enable(powerup);

      powerup.body.setCircle(15);
      powerup.body.kinematic = true;
      powerup.body.data.shapes[0].sensor = true;
      //powerup.body.debug = true;

      powerup.body.onBeginContact.add(this.powerupUse);

      cfg.powerups.list = powerups;
    }
  },
  powerupTake: function(powerup) {
    var cfg = CosmicConfig.config;
    var player = CosmicConfig.config.players.list[CosmicConfig.config.players.current];
    var powerupName = powerup.sprite.name;

    powerup.sprite.kill();

    if (powerupName == 'powerup-health') {
      player.health++;
      console.log('Player Health:', player.health);

    } else if (powerupName == 'powerup-shield') {
      player.health++;
      this.shield = Cosmic.game.add.sprite(player.x, player.y, 'powerup-shield-sprite');
      this.shield.animations.add('shield-rotate', [0, 1, 2, 3], 4, true);
      this.shield.animations.play('shield-rotate', 8, true);
    }
  },
  powerupUse: function(target) {
    //console.log(target);
  },
  update: function() {
    var cfg = CosmicConfig.config;
    var player = CosmicConfig.config.players.list[CosmicConfig.config.players.current];

    if (this.shield) {
      this.shield.animations.sprite.x = player.x - 15;
      this.shield.animations.sprite.y = player.y - 15;
    }
  }
});

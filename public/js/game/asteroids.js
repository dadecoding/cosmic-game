var CosmicAsteroids = new Class({
  initialize: function() {
    this.config = CosmicConfig.config;
    Cosmic.game.load.physics('physicsData', 'images/sprite.json');
    this.preload();
  },
  preload: function() {
    var cfg = this.config;
    var asteroids = cfg.asteroids.list;
    asteroids = Cosmic.game.add.group();
    cfg.asteroids.list = asteroids;

    this.muzzle = Cosmic.game.add.graphics();
    this.muzzle.lineStyle(2, 0xfffffff);
    this.muzzle.moveTo(0, -10);
    this.muzzle.lineTo(0, 10);
    this.muzzle.moveTo(10, 0);
    this.muzzle.lineTo(-10, 0);
    this.muzzle.position.setTo(Cosmic.game.world.centerX, Cosmic.game.world.centerY);
    var muzzle = this.muzzle;

    this.generateAsteroid();

  },
  generateAsteroid: function() {
    var cfg = CosmicConfig.config;
    var asteroids = cfg.asteroids.list;
    var asteroidLevel = Cosmic.game.rnd.integerInRange(1, 3);
    //var asteroid = asteroids.create(Cosmic.game.world.randomX, Cosmic.game.world.randomY, 'asteroid');
    var asteroid;

    if (asteroidLevel == 1) {
      asteroid = asteroids.create(200, 200, 'asteroid');
    } else if (asteroidLevel == 2) {
      asteroid = asteroids.create(200, 200, 'asteroid2');
    } else if (asteroidLevel == 3) {
      asteroid = asteroids.create(200, 200, 'asteroid3');
    }

    Cosmic.game.physics.p2.enable(asteroid);

    asteroid.name = 'asteroid';
    asteroid.level = asteroidLevel;
    asteroid.hp = 3;

    asteroid.body.clearShapes();

    if (asteroidLevel == 1) {
      asteroid.body.loadPolygon('physicsData', 'asteroid');
    } else if (asteroidLevel == 2) {
      asteroid.body.loadPolygon('physicsData', 'asteroid2');
    } else if (asteroidLevel == 3) {
      asteroid.body.loadPolygon('physicsData', 'asteroid3');
    }

    //asteroid.body.debug = true;

    asteroid.body.onBeginContact.add(this.asteroidHit, asteroid);

    cfg.asteroids.list = asteroids;

  },
  asteroidHit: function(target) {
    if (target) {
      if (target.sprite.name == 'bullet') {
        var explosion = Cosmic.game.add.sprite(0, 0, 'explosion');
        explosion.animations.add('explode', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0], 5, false);
        explosion.x = target.sprite.body.x - 70;
        explosion.y = target.sprite.body.y - 70;
        target.sprite.kill();
        explosion.animations.play('explode', 15, false);
      }
    }
  },
  devideAsteroid: function(level) {
    var cfg = CosmicConfig.config;
    var asteroids = cfg.asteroids.list;
    var asteroidName;

    if (level == 2) {
      asteroidName = 'asteroid3';
    } else if (level == 1) {
      asteroidName = 'asteroid2';
    }

    for (var i=0; i != 2; i++) {
      var asteroid = asteroids.create(Cosmic.game.world.randomX, Cosmic.game.world.randomY, asteroidName);
      Cosmic.game.physics.p2.enable(asteroid);

      asteroid.name = 'asteroid';
      asteroid.level = level+1;
      asteroid.hp = 3;

      //asteroid.body.debug = true;

      asteroid.body.clearShapes();
      asteroid.body.loadPolygon('physicsData', asteroidName);

      asteroid.body.onBeginContact.add(this.asteroidHit, asteroid);
    }
  }
});

var CosmicGame = new Class({
  initialize: function() {
    this.game = new Phaser.Game(800, 600, Phaser.AUTO, 'cosmic', {
      preload: this.preload,
      create: this.create,
      update: this.update,
      render: this.render
    });

    this.config = null;
  },
  preload: function() {
    this.config = CosmicConfig.config;

    this.game.load.spritesheet('explosion', 'images/explosion.png', 140, 140);
    this.game.load.image('ship', 'images/player.png');
    this.game.load.image('bg', 'images/space.jpg');
    this.game.load.image('bullet', 'images/bullet.png');

    this.game.load.image('powerup-shield', 'images/powerup-shield.png', 30, 33);
    this.game.load.image('powerup-health', 'images/powerup-health.png', 30, 33);
    this.game.load.spritesheet('powerup-shield-sprite', 'images/powerup-shield-sprite.png', 30, 33);

    this.game.load.spritesheet('asteroid', 'images/asteroid.png', 300, 250);
    this.game.load.spritesheet('asteroid2', 'images/asteroid2.png', 136, 144);
    this.game.load.spritesheet('asteroid3', 'images/asteroid3.png', 75, 69);

    this.game.load.physics('physicsData', 'images/sprite2.json');
  },
  create: function() {
    var cfg = this.config;
    var currentPlayer = cfg.players.list[cfg.players.current];

    // Map
    this.game.physics.startSystem(Phaser.Physics.P2JS);

    this.game.stage.backgroundColor = cfg.game.backgroundColor;
    this.game.add.tileSprite(0, 0, cfg.game.worldWidth, cfg.game.worldHeight, 'bg');

    // Player

    currentPlayer = this.game.add.sprite(384, 287, 'ship');
    this.game.physics.p2.enable(currentPlayer);

    Asteroids = new CosmicAsteroids();
    Shooting = new CosmicShooting();
    Powerups = new CosmicPowerups();

    // Do not pause the game when browser window loses focus
    this.game.stage.disableVisibilityChange = true;

    currentPlayer.body.clearShapes();
    currentPlayer.body.loadPolygon('physicsData', 'ship');

    currentPlayer.body.onBeginContact.add(function(target) {
      if (target) {
        if (target.sprite.name == 'powerup-health' || target.sprite.name == 'powerup-shield') {
          Powerups.powerupTake(target);
        }
      }
    });

    // Controls
    cfg.players.list[cfg.players.current] = currentPlayer;
    cfg.game.cursors = this.game.input.keyboard.createCursorKeys();
  },
  update: function() {
    var cfg = CosmicConfig.config;
    var controls = cfg.game.cursors;
    var currentPlayer = cfg.players.list[cfg.players.current];

    Powerups.update();

    if (controls.left.isDown) {
      currentPlayer.body.rotateLeft(100);
    } else if (controls.right.isDown) {
      currentPlayer.body.rotateRight(100);
    } else {
      currentPlayer.body.setZeroRotation();
    }

    if (controls.up.isDown) {
      currentPlayer.body.thrust(400);
    } else if (controls.down.isDown) {
      currentPlayer.body.reverse(400);
    }

    if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
      Shooting.fire();
    }


    //Cosmic.screenWrap(currentPlayer);


    // shipdata = {
    //   shipname: username,
    //   x: cfg.players.list[cfg.players.current].x,
    //   y: cfg.players.list[cfg.players.current].y,
    //   angle: cfg.players.list[cfg.players.current].angle
    // }
    //
    // socket.emit('ship move', shipdata);
    //
    // socket.on('move ship', function(shipdata) {
    //   shipsList[shipdata.shipname].x = shipdata.x;
    //   shipsList[shipdata.shipname].y = shipdata.y;
    //   shipsList[shipdata.shipname].angle = shipdata.angle;
    // });

    this.game.physics.arcade.collide(currentPlayer, cfg.asteroids.list, null, null, null);
  //  this.game.physics.arcade.collide(cfg.shooting.bullets, cfg.asteroids.list, Shooting.bulletHit, null, this);
  //  this.game.physics.arcade.collide(cfg.asteroids.list, cfg.asteroids.list, null, null, this);

  },
  playerHit: function() {
    Shooting.playerHit();
  },
  render: function() {
    var cfg = this.config;
    var currentPlayer = cfg.players.list[cfg.players.current];
    //console.log(this.game.time.fps);
  },
  screenWrap: function(ship) {
    console.log(ship.x, ship.y);

    if (ship.x < 16) {
      ship.x = this.game.width-20;
    } else if (ship.x > this.game.width-20) {
      ship.x = 16;
    }

    if (ship.y < 15) {
      ship.y = this.game.height-20;
    } else if (ship.y > this.game.height-20) {
      ship.y = 15;
    }
  },
  createShips: function() {
    var cfg = CosmicConfig.config;
    for (var sh in cfg.players.list) {
      if (cfg.players.current != sh) {
        cfg.players.list[sh] = this.game.add.sprite(384, 287, 'ship');
        cfg.players.list[sh].anchor.setTo(0.5);
        this.game.physics.enable(cfg.players.list[sh], Phaser.Physics.P2JS);
        cfg.players.list[sh].body.drag.set(cfg.ship.speed.break);
        cfg.players.list[sh].body.maxVelocity.setTo(cfg.ship.speed.top);
        cfg.players.list[sh].collideWorldBounds = true;
      }
    }
  }
});
